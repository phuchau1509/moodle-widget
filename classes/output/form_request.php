<?php
/**
 * Created by PhpStorm.
 * User: haumaip
 * Date: 1/8/2018
 * Time: 11:11 AM
 */

namespace mod_widget\output;
defined('MOODLE_INTERNAL') || die();
use renderable;
use renderer_base;
use templatable;
use moodle_url;

class form_request implements renderable, templatable
{
    protected $msg;

    public function __construct($msg = '') {
        $this->msg = $msg;
    }

    public function export_for_template(renderer_base $output) {
        $data = [
            'action' => new moodle_url('/mod/widget/form_handler.php'),
            'method' => 'POST',

            'title' => get_string('title', 'mod_widget'),
            'name' => get_string('name', 'mod_widget'),
            'email' => get_string('email', 'mod_widget'),
            'phone' => get_string('phone', 'mod_widget'),
            'submit' => get_string('submit', 'mod_widget'),
            'require' => get_string('require', 'mod_widget'),
            'placeholderforname' => get_string('placeholderforname', 'mod_widget'),
            'placeholderforemail' => get_string('placeholderforemail', 'mod_widget'),
            'placeholderforphone' => get_string('placeholderforphone', 'mod_widget')
        ];
        if (!empty($this->msg))
        {
            if ($this->msg->type === 'success')
            {
                $this->msg->content = get_string('save_success', 'mod_widget');
            }
            //type = error


            array_push($data, ['msg' => $this->msg]);
        }
        return $data;
    }
}