<?php
/**
 * Created by PhpStorm.
 * User: haumaip
 * Date: 1/8/2018
 * Time: 10:53 AM
 */

class User
{
    protected $name;
    protected $email;
    protected $phone;
    protected $course;
    protected $grade = 100;
    protected $timecreated;
    protected $timemodified;

    /**
     * @return mixed
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail ()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone ()
    {
        return $this->phone;
    }

    /**
     * @return int
     */
    public function getGrade (): int
    {
        return $this->grade;
    }

    /**
     * @param int $grade
     */
    public function setGrade (int $grade)
    {
        $this->grade = $grade;
    }

    /**
     * @param mixed $course
     */
    public function setCourse ($course)
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getTimecreated ()
    {
        return $this->timecreated;
    }

    /**
     * @param mixed $timecreated
     */
    public function setTimecreated ($timecreated)
    {
        $this->timecreated = $timecreated;
    }

    /**
     * @return mixed
     */
    public function getTimemodified ()
    {
        return $this->timemodified;
    }

    /**
     * @param mixed $timemodified
     */
    public function setTimemodified ($timemodified)
    {
        $this->timemodified = $timemodified;
    }

    /**
     * @return mixed
     */
    public function getCourse ()
    {
        return $this->course;
    }


    /**
     * @param mixed $name
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail ($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone ($phone)
    {
        $this->phone = $phone;
    }
}