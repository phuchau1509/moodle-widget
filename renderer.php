<?php
/**
 * Created by PhpStorm.
 * User: haumaip
 * Date: 1/8/2018
 * Time: 11:17 AM
 */
class mod_widget_renderer extends plugin_renderer_base
{
    public function render_form_request (\mod_widget\output\form_request $data) {
        return $this->render_from_template('mod_widget/form_request', $data->export_for_template($this));
    }
}