<?php
/**
 * Created by PhpStorm.
 * User: haumaip
 * Date: 1/8/2018
 * Time: 10:40 AM
 */
require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once (dirname(__FILE__).'/classes/objects/User.php');
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone'])) {
    $user = new User();
    $user->setName($_POST['name']);
    $user->setEmail($_POST['email']);
    $user->setPhone($_POST['phone']);
    widget_insert_recommend($user);
} else {
    echo 'Data invalid!';
}