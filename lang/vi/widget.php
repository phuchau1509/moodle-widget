<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for widget
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_widget
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'widget';
$string['modulenameplural'] = 'widgets';
$string['modulename_help'] = 'Sử dụng mô-đun widget cho ... | Mô-đun widget cho phép ...';
$string['widget:addinstance'] = 'Thêm tiện ích mới';
$string['widget:submit'] = 'Gửi tiện ích';
$string['widget:view'] = 'Xem tiện ích';
$string['widgetfieldset'] = 'Trường dữ liệu ví dụ tuỳ chỉnh';
$string['widgetname'] = 'tên tiện ích con';
$string['widgetname_help'] = 'Đây là nội dung của chú giải công cụ trợ giúp được liên kết với trường widgetname. Cú pháp Markdown được hỗ trợ.';
$string['widget'] = 'widget';
$string['pluginadministration'] = 'quản lý widget';
$string['pluginname'] = 'widget';

$string['title'] = 'Form của tôi';
$string['name'] = 'Tên';
$string['email'] = 'Email';
$string['phone'] = 'Số điện thoại';
$string['submit'] = 'Gửi đi';
$string['require'] = 'Bắt buộc';

$string['placeholderforname'] = 'Họ và tên của bạn';
$string['placeholderforemail'] = 'example@domain.com';
$string['placeholderforphone'] = 'Số điện thoại của bạn';